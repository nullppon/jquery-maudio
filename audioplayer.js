(function() {
	"use strict";

	window.AudioContext = window.AudioContext || window.webkitAudioContext;
	var context = new AudioContext();
	var request, source, context, buffer;
	var flag = true;
	var startTime, replayTime, pauseTime, pausingTime, playTime;
	var dataset;

	window.onload = function() {
		request = new XMLHttpRequest();
		request.open('GET', 'sgs_top.mp3', true);
		request.responseType = 'arraybuffer';
		request.onload = function() {
			if (request.status === 200)
			{
				var arrayBuffer = request.response;
				if (arrayBuffer instanceof ArrayBuffer)
				{
					var successCallback = function(audioBuffer) {
						buffer = audioBuffer;
					};

					var errorCallback = function() {
						console.log('読み込みに失敗しました');
					};

					context.decodeAudioData(arrayBuffer, successCallback, errorCallback);
				}
			}

			var elem = document.getElementById('playStart');
			elem.addEventListener('click', playStart, false);
			var elemStop = document.getElementById('playStop');
			elemStop.addEventListener('click', playStop, false);
		};
		request.send();
	};

	function playStart() {
		var elem = document.getElementById('playStart');
		dataset = elem.dataset;
		if (dataset.audio === 'true')
		{
			dataset.audio = 'false';
			source = context.createBufferSource();
			source.buffer = buffer;
			source.loop = true;
			source.loopStart = 2.81694;
			source.loopEnd = 60.93489;
			source.connect(context.destination);
			if (flag === true)
			{
				startTime = context.currentTime;
				replayTime = startTime;
				pausingTime = 0;
				flag = false;
			}
			else
			{
				replayTime = context.currentTime;
				pausingTime += replayTime - pauseTime;
			}

			playTime = replayTime - startTime - pausingTime;

			source.start(0, playTime);
		}
		else
		{
			dataset.audio = 'true';
			playPause();
		}
	}

	function playPause() {
		pauseTime = context.currentTime;
		source.stop(0);
	}

	function playStop() {
		dataset.audio = 'true';
		flag = true;
		source.stop();
	}
})();


// (function(d) {
// 	"use strict";

// 	var audio = new Audio();

// 	audio.src = 'sgs_top.mp3';
// 	audio.loop = true || false;
// 	audio.load();

// 	var btnStart = d.getElementById('playStart2');
// 	var audioPlayer = d.getElementById('audioObject');
// 	btnStart.onclick = function() {
// 		audio.play();
// 	};

// 	var btnPause = d.getElementById('playStop2');
// 	btnPause.onclick = function() {
// 		audio.pause();
// 	};


// })(document);
