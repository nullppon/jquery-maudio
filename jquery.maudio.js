/***************************************************
Plugin Name : jQuery mAudio
Description : Web Audio APIを使った再生と一時停止機能だけのmp3再生プラグイン。ループ設定も可能。制作時のjQueryバージョン「1.12.4」
Version     : 1.0.1
Author      : MIRAI Inc.
Since       : 2017/05/24
***************************************************/
;(function($) {
	/**
	 * Web Audio API Play to mp3 file
	 *
	 * @param String filename [file/to/path] relative path
	 * @param Object options
	 */
	$.fn.mAudio = function(filename, options) {
		"use strict";

		// Defaults Setting
		var defaults = {
			'loop'      : true,
			'loopStart' : 0,
			'loopEnd'   : 0,
			'labelBool' : false,
			'label'     : {
				'play'  : 'PLAY',
				'pause' : 'PAUSE',
			},
			'autoplay': false,
		};

		var elem    = this;
		var options = $.extend(true, {}, defaults, options);

		window.AudioContext = window.AudioContext || window.webkitAudioContext;

		var context = new AudioContext();
		var request, source, buffer;

		var flag = true;
		var startTime, replayTime, pauseTime, pausingTime, playTime;

		var visibilityFlag = false;
		var playFlag = false;

		/**
		 * Onload Request
		 */
		$(window).on('load', function() {
			"use strict";

			if (source) return false;
			request = new XMLHttpRequest();
			request.open('GET', filename, true);
			request.responseType = 'arraybuffer';
			request.onload = function() {
				if (request.status === 0 || request.status === 200)
				{
					var arrayBuffer = request.response;
					if (arrayBuffer instanceof ArrayBuffer)
					{
						var successCallback = function(audioBuffer) {
							buffer = audioBuffer;
							var maudioData = elem.data('maudio');

							if (typeof maduioData === 'undefined' || maudioData === false)
							{
								elem.data('maudio', 'true');
								elem.each(function(i) {
									if ($(this).data('maudio-label') === true)
									{
										$(this).html(options.label.play);
									}
								});
							}
							if (options.autoplay === true)
							{
								playFlag = true;
								audioPlay(buffer);
							}
						};

						var errorCallback = function() {
							console.error('読み込みに失敗しました');
						};

						// Audio Decode
						context.decodeAudioData(arrayBuffer, successCallback, errorCallback);
					}
				}
				else
				{
					alert(filename + ' が取得できませんでした');
				}

				function audioPlay(_buffer)
				{
					elem.data('maudio', 'false');

					// Audio source create buffer
					source           = context.createBufferSource();
					source.buffer    = _buffer;
					source.loop      = options.loop;
					source.loopStart = options.loopStart;
					source.loopEnd   = options.loopEnd;

					// Audio Source connect
					source.connect(context.destination);

					if (flag === true)
					{
						startTime   = context.currentTime;
						replayTime  = startTime;
						pausingTime = 0;
						flag        = false;
					}
					else
					{
						replayTime   = context.currentTime;
						pausingTime += replayTime - pauseTime;
					}

					playTime = replayTime - startTime - pausingTime;

					// Button change label.
					elem.each(function(i) {
						if ($(this).data('maudio-label') === true)
						{
							$(this).html(options.label.pause);
						}
					});

					elem.addClass('active');
					elem.addClass('played');

					// Play audio
					source.start(0, playTime);
				}

				function audioStop(_buffer)
				{
					elem.data('maudio', 'true');

					// Button change label.
					elem.each(function(i) {
						if ($(this).data('maudio-label') === true)
						{
							$(this).html(options.label.play);
						}
					});

					elem.removeClass('active');
					elem.removeClass('played');

					// Pause audio
					if (typeof source !== 'undefined')
					{
						source.stop(0);
					}
				}

				document.addEventListener("visibilitychange", function(event) {
					if (visibilityFlag || playFlag === false) return;
					if (document.visibilityState === 'hidden')
					{
						// Pause audio set currentTime
						pauseTime = context.currentTime;
						audioStop(buffer);
					}
					else
					{
						audioPlay(buffer);
					}
				});

				/**
				 * onClick play
				 */
				elem.on('click', function() {
					"use strict";

					var maudioData = elem.data('maudio');

					if (maudioData === 'true')
					{
						visibilityFlag = false;
						playFlag = true;
						audioPlay(buffer);
					}
					else
					{
						visibilityFlag = true;
						playFlag = false;
						// Pause audio set currentTime
						pauseTime = context.currentTime;
						audioStop(buffer);
					}
				});
			};
			request.send();
		});

		return this;
	};
})(jQuery);
